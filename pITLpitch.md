---
marp: true

title: Simulação digital de um pedal de guitarra
description: Flávio Henrique Licorio Leiva
Samuel Licorio Leiva
paginate: true
_paginate: false



---

# <!--fit-->Simulação digital de um pedal de guitarra

Flávio Henrique Licorio Leiva
Samuel Licorio Leiva

#### Professor orientador: Éttore Leandro Tognoli

---

<!--
_backgroundColor: #123
_color: #fff
-->

## Proposta

Simular digitalmente o efeito do pedal BOOS DS-1 ou JF 01 Vintage Overdrive

---

<!--
_backgroundColor: #123
_color: #fff
_contentAlign: center
-->
## Estratégias de solução

#### Solução através de algoritmos matemáticos

LTI - Linaer time-invariant 
![LTI](320px-LTI.png)

---

<!--
_backgroundColor: #123
_color: #fff
-->

## Estratégias de solução

#### Solução através de aprendizado de máquina
