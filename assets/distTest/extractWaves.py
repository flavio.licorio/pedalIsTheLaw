import fnmatch
import os
import csv
import scipy
from scipy.io import wavfile
from sklearn.neural_network import MLPRegressor

def recursive_files():
    matches = []
    for root, dirnames, filenames in os.walk('./'):
        for filename in fnmatch.filter(filenames, '*.wav'):
            matches.append(os.path.join(root, filename))
    return matches
    # wavfile.read('')

all_waves = list()

files = recursive_files()

for file in files:
    name = file.split('/')[-1]
    fs, signal =  wavfile.read(name)
    secs = signal.shape[0] / float(fs)
    Ts = 1.0/fs
    t = scipy.arange(0,secs,Ts)
    try: 
        insert = [name, list(signal[0:800])]
        all_waves.append(insert)
    except IndexError:
        print(IndexError)
        continue

X = []
y = []

for line in all_waves:
    if("pure" in line[0]):
        X.append(line[1])
        print("pure")
    elif("dist" in line[0]):
        y.append(line[1])
        print("dist")
    elif ("train" in line[0]):
        x_train = line[1]
        print("train")
    else:
        y_test = line[1]
        print("test")



clf = MLPRegressor(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(600, 400, 600), random_state=1, max_iter=400)
print("Regressor")

clf.fit(X,y)
print("fit")

y_train = clf.predict([x_train[0:800]])
print("predict")

print(len(y_train[0]))

# with open('waves.csv', 'w') as csv_file:
#     writer = csv.writer(csv_file, delimiter=';')
#     for line in zip(*all_waves):
#         print(line)
#         writer.writerow(line)